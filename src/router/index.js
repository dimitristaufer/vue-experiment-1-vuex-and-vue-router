import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home";
import store from '../store/index.js'



// tell Vue to use the router
Vue.use(VueRouter);

const routes = [
    {
        path: "/", // dynamic segment in the path with :
        // named routes, means we can change the path, but don't need to change anything else
        name: "Home",
        // lazy loading
        component: Home,
        props: true,
        // nested routes are displayed n the Home-page
        children: [
            {
                path: "/country/:slug",
                name: "CountryDetails",
                component: () =>
                    import(
                        /* webpackChunkName: "details" */ "../views/CountryDetails"
                        ),
                props: true,
                //meta for navigation guard for child routes
                //meta: {countyAvailable: true}
            },
        ],
    },

    {
        path: "/about",
        name: "About",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/About"
                ),
        props: true,

    },
    {
        path: "/404",
        alias: "*",
        name: "NotFound",
        component: () =>
            import(
                /* webpackChunkName: "notfound" */ "../views/NotFound"
                ),
        props: true,

    },
]


//for URL without hashes
const mode = "history";

const router = new VueRouter({
    routes,
    mode
});

//navigation guard but better suited for auth.
router.beforeEach((to, from, next) =>{
    if(to.matched.some(record => record.meta.countyAvailable)){
        const exists = (store.getters.getAllCoronaData).find(countryData => String(countryData.Slug) === to.params.slug);
        if(exists){
            next();
        }
        else{
            next('/404')
        }
    }
    else{
        next()
    }

})

export default router;
