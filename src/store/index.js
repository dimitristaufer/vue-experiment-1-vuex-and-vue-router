import Vue from "vue";
import Vuex from "vuex";
import singleModule from "./modules/singleModule";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    singleModule,
  },
});
