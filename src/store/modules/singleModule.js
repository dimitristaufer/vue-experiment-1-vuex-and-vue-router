// initial state
const state = () => ({
  coronaData: [],
  coronaDataFiltered: [],
  loading: false,
  error: null,
  searchQuery: "",
});

// getters are like computed properties, but for stores
const getters = {
  getAllCoronaData(state) {
    return state.coronaData;
  },
  getFilteredCoronaData(state) {
    return state.coronaDataFiltered;
  },
  getSearchQuery(state) {
    return state.searchQuery;
  },
  getLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
};

// asynchronous actions that commit mutations to the synchronous mutation handler
const actions = {
  async downloadCoronaData({ commit }) {
    /*  ES2015 argument destructuring.
        Without this, it would be:
        fetchData(context) {}... context.commit()
    */

    commit("setLoading", true);

    fetch("https://api.covid19api.com/summary")
      .then((response) => response.json()) // turn the fetched text data into a json object
      .then((data) => {
        setTimeout(() => {
          let rawCoronaData = data["Countries"]; // extract the 'Countries' list in the data dictionary
          if (rawCoronaData != null) {
            rawCoronaData.sort(function(a, b) {
              // sort the data by the number of newly confirmed cases
              if (a.NewConfirmed < b.NewConfirmed) return 1;
              if (a.NewConfirmed > b.NewConfirmed) return -1;
              return 0;
            });
          }
          commit("setCoronaData", rawCoronaData);
          //commit("filterCoronaData", "");
          commit("setLoading", false);
          commit("setError", null);
        }, 1000);
      })
      .catch((error) => {
        commit("setLoading", false);
        commit("setError", error);
      });
  },
};

// synchronous mutation handler
const mutations = {
  setCoronaData(state, coronaData) {
    state.coronaData = coronaData;
    state.coronaDataFiltered = coronaData;
  },
  filterCoronaData(state, searchQuery) {
    state.searchQuery = searchQuery;
    if (searchQuery === "") {
      state.coronaDataFiltered = state.coronaData;
    } else {
      state.coronaDataFiltered = state.coronaData.filter(function(a) {
        return (
          a.Country.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1
        );
      });
    }
  },
  setLoading(state, loading) {
    state.loading = loading;
  },
  setError(state, error) {
    state.error = error;
  },
};

// Exports some important properties
export default {
  state,
  getters,
  actions,
  mutations,
};
